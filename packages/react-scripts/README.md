# react-scripts

This package includes scripts and configuration used by [VIU-react-starter](https://github.com/facebook/create-react-app). VIU-react-starter is an extention of create-react-app. All the features available in Create-react-app will still be available in this extention, along with some extra features commonly used at VUClip User Interface projects.<br>
Please refer to its documentation:

- [Getting Started](https://github.com/facebook/create-react-app/blob/master/README.md#getting-started) – How to create a new app.
- [User Guide](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md) – How to develop apps bootstrapped with Create React App.
