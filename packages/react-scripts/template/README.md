This project has been extended from [Create React App](https://github.com/facebook/create-react-app) specifically for common needs at VUClip.

Please contact [Mahesh Kedari](mailto:mahesh.kedari@vuclip.com) for any features to be added/removed from this code generator.
