import React from 'react';
import PropTypes from 'prop-types';
import Styles from './breadcrumb.css';
import BreadcrumbItem from './breadcrumbItem.js';

const Breadcrumb = () => {
  return (
    <div className={Styles.breadcrumbContainer}>
      <BreadcrumbItem />
    </div>
  );
};

Breadcrumb.propTypes = {};

export default Breadcrumb;
