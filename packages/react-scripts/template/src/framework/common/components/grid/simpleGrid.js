import React from 'react';
import PropTypes from 'prop-types';
import GridRow from './gridRow';
import Styles from './grid.css';

const SimpleGrid = () => {
  return (
    <div>
      <GridRow />
    </div>
  );
};

SimpleGrid.propTypes = {};

export default SimpleGrid;
