import React from 'react';
import PropTypes from 'prop-types';
import Styles from './notification.css';

const Notification = () => {
  return <div className={Styles.notificationContainer} />;
};

Notification.propTypes = {};

export default Notification;
