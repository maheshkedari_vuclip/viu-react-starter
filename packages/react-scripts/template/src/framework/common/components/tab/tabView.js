import React from 'react';
import PropTypes from 'prop-types';
import Tab from './tab';
import Styles from './tabView.css';

const TabView = () => {
  return <div className={Styles.tabViewContainer} />;
};

TabView.propTypes = {};

export default TabView;
