import React from 'react';
import Styles from './content.module.css';

const Content = () => {
  return (
    <div className={Styles.content}>
      <div className="container-fluid">
        <h1>Page Content Here</h1>
      </div>
    </div>
  );
};

export default Content;
