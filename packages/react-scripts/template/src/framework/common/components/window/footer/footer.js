import React from 'react';
import Styles from './footer.module.css';

const Footer = () => {
  return (
    <div className={Styles.footer_container}>
      &copy; VIU 2018. All Rights Reserved
    </div>
  );
};

export default Footer;
