import React from 'react';
import PropTypes from 'prop-types';
import UserDetails from './user.details';

import Styles from './header.module.css';
import Title from './title';
import Logo from './logo';

const Header = () => {
  return (
    <div className={Styles.header_container}>
      <Logo />
      <Title />
      <UserDetails />
    </div>
  );
};

Header.propTypes = {};
export default Header;
