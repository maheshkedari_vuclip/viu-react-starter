import React from 'react';
import PropTypes from 'prop-types';
import Styles from './header.module.css';
import { CONSTANTS } from '../../../../constants/framework.constants';

const Logo = () => {
  return (
    <div className={Styles.logo_container}>
      <img
        src={CONSTANTS.logo}
        alt="vuclip Smart Alert"
        className="mCS_img_loaded"
      />
    </div>
  );
};

Logo.propTypes = {};
export default Logo;
