import React from 'react';
import PropTypes from 'prop-types';
import Styles from './header.module.css';

const Title = () => {
  return (
    <div className={Styles.title_container}>
      <h2>Title Component</h2>
    </div>
  );
};

Title.propTypes = {};
export default Title;
