import React from 'react';
import PropTypes from 'prop-types';
import Styles from './header.module.css';

const UserDetails = () => {
  return <div className={Styles.userdetails_container}>User Details</div>;
};

UserDetails.propTypes = {};

export default UserDetails;
