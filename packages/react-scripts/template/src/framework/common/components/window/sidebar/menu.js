import React from 'react';
import PropTypes from 'prop-types';

import MenuItem from './menuitem';

const Menu = props => {
  const { menuitems } = props;
  var menurenderedItems = menuitems.map((item, index) => (
    <MenuItem item={item} key={'sidemenu_' + index} />
  ));
  return <ul>{menurenderedItems}</ul>;
};

Menu.propTypes = {
  menuitems: PropTypes.array,
};

export default Menu;
