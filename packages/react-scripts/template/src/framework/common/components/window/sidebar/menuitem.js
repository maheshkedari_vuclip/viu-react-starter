import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';

const MenuItem = props => {
  const { item } = props;

  return (
    <li>
      <a href={item.path}>
        <Icon name={item.icon} size="small" />
        <span>{item.title}</span>
      </a>
    </li>
  );
};

MenuItem.propTypes = {
  item: PropTypes.shape({
    path: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    icon: PropTypes.string,
  }),
};

export default MenuItem;
