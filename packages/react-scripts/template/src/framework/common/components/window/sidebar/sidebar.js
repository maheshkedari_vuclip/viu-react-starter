import React from 'react';
import PropTypes from 'prop-types';
import styles from './sidebar.module.css';
import Menu from './menu';

const Sidebar = () => {
  const menuitems = [
    {
      title: 'Application Name',
      icon: 'home',
      path: '/appname',
    },
    {
      title: 'Home',
      icon: 'home',
      path: '/appname',
    },
    {
      title: 'Configurations',
      icon: 'configure',
      path: '/appname',
    },
    {
      title: 'Publish',
      icon: 'home',
      path: '/appname',
    },
    {
      title: 'History',
      icon: 'history',
      path: '/history',
    },
  ];

  return (
    <div className={styles.sidebar}>
      <Menu menuitems={menuitems} />
    </div>
  );
};

Sidebar.propTypes = {};

export default Sidebar;
