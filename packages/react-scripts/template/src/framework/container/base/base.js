import React, { Component } from 'react';
import {
  Header,
  Sidebar,
  Content,
  Footer,
} from '../../common/components/window';
import Styles from './base.module.css';

export default class Base extends Component {
  render() {
    return (
      <div className={Styles.base}>
        <Header />
        <Sidebar />
        <Content />
        <Footer />
      </div>
    );
  }
}
