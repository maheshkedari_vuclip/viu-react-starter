import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import DevTools from './devTools';
import { Route } from 'react-router-dom';
import Base from './base/base';
import Login from './authentication/login';
import Logout from './authentication/logout';

import Styles from './container.module.css';

const ShowDevTools = () => {
  if (process.env.NODE_ENV === 'production') {
    return null;
  } else {
    return <DevTools />;
  }
};

const Root = ({ store }) => (
  <Provider store={store}>
    <div className={Styles.container}>
      <Route path="/" component={Base} />
      <Route path="/login" component={Login} />
      <Route path="/logout" component={Logout} />
      {ShowDevTools()}
    </div>
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired,
};

export default Root;
